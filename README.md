﻿
<!--#echo json="package.json" key="name" underline="=" -->
@instaffogmbh/read-file-if-exists
=================================
<!--/#echo -->

<!--#echo json="package.json" key="description" -->
Try to read a file. Ignore ENOENT.
<!--/#echo -->

* 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).



API
---

This module exports one function:

### readFileIfExists(path[, options])

Arguments are expected as described for
[Node's fs.readFile][nodejs-api-fs-readfile].

Returns a promise that…
* resolves to the file's contents if it the read attempt succeeded.
* resolves to `false` if error `ENOENT` (file not found) was encountered.
* rejects for all other errors.



<!--#toc stop="scan" -->


&nbsp;

[nodejs-api-fs-readfile]: https://nodejs.org/api/fs.html#fs_fs_readfile_path_options_callback



License
-------
<!--#echo json="package.json" key=".license" -->
MIT
<!--/#echo -->
