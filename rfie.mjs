import promisedFs from 'nofs'

function check404(err) {
  if ((err.code === 'ENOENT') && (err.syscall === 'open')) { return false }
  throw err
}

function readFileIfExists(...args) {
  return promisedFs.readFile(...args).then(null, check404)
}

async function withStat(fn, ...args) {
  const data = await readFileIfExists(fn, ...args)
  if (data === false) { return data }
  const stat = await promisedFs.stat(fn)
  return { data, ...stat }
}


Object.assign(readFileIfExists, {
  withStat,
})

export default readFileIfExists
