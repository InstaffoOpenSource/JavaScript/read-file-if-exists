import test from 'p-tape'
import absdir from 'absdir'

import readFileIfExists from '../rfie.node'

const inModDir = absdir(import.meta, '..')

function maybeReadUTF8(fn) { return readFileIfExists(inModDir(fn), 'UTF-8') }

test('basics', async(t) => {
  t.plan(2)
  const data = await maybeReadUTF8('.gitignore')
  t.strictEqual(data.slice(0, 7), '\n/node_')
  const nope = await maybeReadUTF8('no_such_file')
  t.strictEqual(nope, false)
})
